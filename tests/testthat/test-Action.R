testthat::context("Action: defining actions for the automaton to perform")

.newAction <- function(actionFunction, significant) automation:::Action$new(
    nameAsAString = "Dummy action",
    significant = significant,
    initializingFunction = function(context) {
        context[["INITIALIZING"]] <- 1
        return(context)
    },
    actionFunction = actionFunction,
    finalizingFunction = function(context) {
        context[["FINALIZING"]] <- 3
        return(context)
    }
)

testthat::test_that("Action: the constructor works", {
    ac <- .newAction(
        function(context) {
            context[["ACTION"]] <- 2
            return(context)
        },
        FALSE
    )
    testthat::expect_equal(ac$name, "Dummy action")
    testthat::expect_false(ac$significant)
    ctx <- ac$action(context = automation:::Context$new())
    testthat::expect_equal(ctx$INITIALIZING, 1)
    testthat::expect_equal(ctx$ACTION, 2)
    testthat::expect_equal(ctx$FINALIZING, 3)
    testthat::expect_true(ctx$continue())
})

testthat::test_that("Action: the context is handled correctly", {
    ac <- .newAction(
        function(context) {
            context$stop()
            return(context)
        },
        TRUE
    )
    ctx <- automation:::Context$new()
    testthat::expect_true(ctx$continue())
    ctx <- ac$action(context = ctx)
    testthat::expect_true(ctx$status()$isOk())
    testthat::expect_false(ctx$continue())
})

