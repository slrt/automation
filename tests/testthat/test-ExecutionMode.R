testthat::context("ExecutionMode: make it synchronous or asynchronous")

testthat::test_that("ExecutionMode: can compare them", {
    testthat::expect_true(automation:::ExecutionMode$SYNCHRONOUS$equals(automation:::ExecutionMode$SYNCHRONOUS))
    testthat::expect_true(automation:::ExecutionMode$ASYNCHRONOUS$equals(automation:::ExecutionMode$ASYNCHRONOUS))

    testthat::expect_false(automation:::ExecutionMode$SYNCHRONOUS$equals(automation:::ExecutionMode$ASYNCHRONOUS))
    testthat::expect_false(automation:::ExecutionMode$ASYNCHRONOUS$equals(automation:::ExecutionMode$SYNCHRONOUS))
    testthat::expect_false(automation:::ExecutionMode$SYNCHRONOUS$equals(NULL))
    testthat::expect_false(automation:::ExecutionMode$ASYNCHRONOUS$equals(NULL))
})
