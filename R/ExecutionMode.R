#' Moore machine execution mode
#'
#' This is a utility that helps the user providing the expected \code{executionMode} parameter to the \code{MooreMachine$new} constructor.
#'
#' @section List of execution modes:
#' \enumerate{
#'     \code{\link{ExecutionMode$SYNCHRONOUS}}: a synchronous machine expects completion in a deterministic time lapse after having been launched.\cr
#'     \code{\link{ExecutionMode$ASYNCHRONOUS}}: an asynchronous machine expects having to wait for some things to happen. Therefore, each time it is being launched, it executes only one step if able.\cr
#' }
#'
#' @export ExecutionMode
#' @name ExecutionMode
#' @rdname ExecutionMode
ExecutionMode <- (function() {
    ret0 <- list()

    .equals <- function(firstExecutionMode, secondExecutionMode) {
        if (is.null(firstExecutionMode[["name"]])) {
            return(FALSE)
        }
        if (is.null(secondExecutionMode[["name"]])) {
            return(FALSE)
        }
        return(firstExecutionMode$name == secondExecutionMode$name)
    }

    ret0$SYNCHRONOUS <- (function() {
        ret1 <- list(
            "name" = "synchronous execution mode",
            "description" = "The automata expects all actions to be performed at once, without having to wait for answers. Must have an accepting state at least."
        )

        ret1$equals <- function(otherExecutionMode) .equals(ret1, otherExecutionMode)

        return(ret1)
    })()

    ret0$ASYNCHRONOUS <- (function() {
        ret1 <- list(
            "name" = "asynchronous execution mode",
            "description" = "The automata performs exactly one step at a time, expecting a delayed answers. Doesn't have to have an accepting state."
        )

        ret1$equals <- function(otherExecutionMode) .equals(ret1, otherExecutionMode)

        return(ret1)
    })()

    return(ret0)
})()
#' Synchronous execution mode for a machine
#'
#' A synchronous machine expects completion in a deterministic time lapse after having been launched. As such, it waits for each step to finish before calling the next one. A typical use case is a client-side process like a data processing steps, or a deterministic algorithm.\cr
#'
#' @examples
#' machine <- MooreMachine$new(
#'     executionMode = ExecutionMode$SYNCHRONOUS
#' )
#'
#' @name ExecutionMode$SYNCHRONOUS
NULL
#' Asynchronous execution mode for a machine
#'
#' An asynchronous machine expects having to wait for some things to happen. Therefore, each time it is being launched, it executes only one step if able. A typical use case is a series of client-side operations alternated with server side operations.\cr
#'
#' @examples
#' machine <- MooreMachine$new(
#'     executionMode = ExecutionMode$ASYNCHRONOUS
#' )
#'
#' @name ExecutionMode$ASYNCHRONOUS
NULL
